from django.db import models


class Book(models.Model):
    book_genres = (('Fantasy', 'Fantasy'), ('Romance', 'Romance'), ('Horror', 'Horror'), ('SF', 'SF'),
                   ('Mystery', 'Mystery'), ('Other', 'Other'))

    book_title = models.CharField(max_length=30)
    author_name = models.CharField(max_length=30)
    cover = models.CharField(max_length=100)
    year = models.IntegerField()
    description = models.TextField()
    buy_link = models.CharField(max_length=100)
    genre = models.CharField(max_length=7, choices=book_genres)

    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.book_title} / {self.author_name}'
