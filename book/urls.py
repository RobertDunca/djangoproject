from django.urls import path
from book import views

urlpatterns = [
    path('create-new-book/', views.BookCreateView.as_view(), name='create_new_book'),
    path('list-of-books/', views.BookListView.as_view(), name='all_books'),
    path('update-book/<int:pk>/', views.BookUpdateView.as_view(), name='update_book')
]
