from django import forms
from django.forms import TextInput, Textarea, Select

from book.models import Book


class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = ['book_title', 'author_name', 'cover', 'year', 'description', 'buy_link', 'genre']
        widgets = {
            'book_title': TextInput(attrs={'placeholder': 'Please enter book title', 'class': 'form-control'}),
            'author_name': TextInput(attrs={'placeholder': 'Please enter author name', 'class': 'form-control'}),
            'cover': TextInput(attrs={'placeholder': 'Please enter cover image source', 'class': 'form-control'}),
            'year': TextInput(attrs={'placeholder': 'Please enter year of publication', 'class': 'form-control'}),
            'description': Textarea(attrs={'placeholder': 'Please enter description', 'class': 'form-control'}),
            'buy_link': TextInput(attrs={'placeholder': 'Please enter buy link', 'class': 'form-control'}),
            'genre': Select(attrs={'class': 'form-select'})
        }
