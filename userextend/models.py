from django.contrib.auth.models import User
from django.db import models

# age, gender(MALE, FEMALE, OTHER), phone, email_confirmation, mailing_address


class UserExtend(User):
    gender_options = (('male', 'Male'), ('female', 'Female'), ('other', 'Other'))

    age = models.IntegerField()
    phone = models.CharField(max_length=30)
    email_confirmation = models.EmailField(max_length=30)
    mailing_address = models.CharField(max_length=100)
    gender = models.CharField(max_length=6, choices=gender_options)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'
