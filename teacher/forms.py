from django import forms
from django.forms import TextInput, EmailInput, Textarea, DateTimeInput, Select

from teacher.models import Teacher


class TeacherForm(forms.ModelForm):
    class Meta:
        model = Teacher
        fields = ['first_name', 'last_name', 'department', 'course', 'description',
                  'start_date', 'end_date']
        widgets = {
            'first_name': TextInput(attrs={'placeholder': 'Please enter your first name', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Please enter your last name', 'class': 'form-control'}),
            'department': Select(attrs={ 'class': 'form-control'}),
            'course': TextInput(attrs={'placeholder': 'Please enter your course', 'class': 'form-control'}),
            'description': Textarea(attrs={'placeholder': 'Please enter your description', 'class': 'form-control'}),
            'start_date': DateTimeInput(attrs={'class' : 'form-control', 'type': 'datetime-local'}),
            'end_date': DateTimeInput(attrs={'class': 'form-control', 'type': 'datetime-local'}),
        }
