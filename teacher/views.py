# from django.shortcuts import render
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DetailView, DeleteView

from student.models import Student
from teacher.forms import TeacherForm
from teacher.models import Teacher


class TeacherCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'teacher/create_teacher.html'
    model = Teacher
    # fields = '__all__'
    form_class = TeacherForm
    success_url = reverse_lazy('create_new_teacher')


class TeacherListView(LoginRequiredMixin, ListView):
    template_name = 'teacher/list_of_teachers.html'
    model = Teacher
    context_object_name = 'all_teachers'


class TeacherUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'teacher/update_teacher.html'
    model = Teacher
    form_class = TeacherForm
    success_url = reverse_lazy('all_teachers')


class TeacherDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = 'teacher/details_teacher.html'
    model = Teacher


class TeacherDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'teacher/delete_teacher.html'
    model = Teacher
    success_url = reverse_lazy('all_teachers')


@login_required
@permission_required('inactivate_teacher')
def inactive_teacher(request, pk):
    Teacher.objects.filter(id=pk).update(active=False)
    return redirect('all_teachers')


def get_all_students_per_teacher(request, pk):  # PK INSEMNAND id.ul profesorului
    all_students_per_teacher = Student.objects.filter(teacher_id=pk)
    context = {'all_students_per_teachers': all_students_per_teacher}
    return render(request, 'teacher/all_students_per_teacher.html', context)
